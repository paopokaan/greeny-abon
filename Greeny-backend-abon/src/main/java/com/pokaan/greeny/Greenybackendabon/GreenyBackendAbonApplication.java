package com.pokaan.greeny.Greenybackendabon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreenyBackendAbonApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreenyBackendAbonApplication.class, args);
	}
}
